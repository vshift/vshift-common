
module.exports = {

  allowCompoundWords: true,

  dictionaries: [
    // see https://github.com/streetsidesoftware/cspell-dicts/tree/main/dictionaries
    'en_US',
    'companies',
    'softwareTerms',
    'misc',
    'typescript',
    'node',
    'html',
    'html-symbol-entities',
    'css',
    'fonts',
    'filetypes',
    'npm',
    'lorem-ipsum',
  ],

  words: [

    // companies
    'vshift',
    'contentful',
    'crownpeak',
    'vaneck',
    'pgim',
    'insperity',

    // names
    'hetherington',

    // software
    'zeplin',

    // npm packages
    'anymatch',
    'corejs',
    'esbuild',
    'hasha',
    'reactour',
    'stylelint',

    // fonts
    'avenir',
    'monokai',
    'oldstyle',
    'opentype',
    'poynter',

    // html/js/css
    'centerscreen',
    'href',
    'hrefs',
    'nostrip',
    'valign',
    'zindex',

    // webpack
    'icss',

    // emoji
    'hankey',
    'tada',

    // misc words and 'words'
    'elems',
    'focusables',
    'headshot',
    'navs',
    'subnav',
    'questionee',
    'responsivize',
    'typeahead',
    'unclass',
    'unclassed',
    'unstyle',
    'unstyled',
    'val',
    'vals',
    'vcard',
    'wordmark',
  ],

  ignorePaths: [
    'package.json',
  ],

  ignoreRegExpList: [
    // TODO: break these up into file type specific configs
    `require\\('.+?'\\);$`, // npm requires
    `from '.+?';$`, // npm imports
    `@import '.+?';$`, // scss imports
    '&\\w+;', // html entities
  ],
};

