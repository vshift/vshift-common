module.exports = {
  printWidth: 100,
  // tabWidth: 2,
  // useTabs: false,
  // semi: true,
  singleQuote: true,
  // quoteProps: 'as-needed',
  // jsxSingleQuote: false,
  trailingComma: 'all',
  // bracketSpacing: true,
  // jsxBracketSameLine: false,
  // arrowParens: 'avoid',
  // rangeStart:
  // rangeEnd:
  // parser:
  // filePath:
  // requirePragma: false,
  // insertPragma: false,
  // proseWrap: 'preserve',
  // htmlWhitespaceSensitivity: 'css',
  // vueIndentScriptAndStyle: false,
  endOfLine: 'lf',
};
