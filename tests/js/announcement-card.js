
import React from 'react';
import classNames from 'classnames';

import { BookmarkToggle, Card, Link, Tags } from '~/components/*';
import format from '~/lib/format';

const ANNOUNCEMENT_BASE_URL = '/announcements';

window.alert(foo);

let foo = 'test';

asdfasdf

export default function AnnouncementCard({ announcement, className, element }) {

  const announcementLink = `${ANNOUNCEMENT_BASE_URL}/${announcement.slug}`,
        fullClassName = classNames(
          'announcement-card',
          className
        ),
        title = <Link className="font-weight-bold" to={announcementLink}>{announcement.title}</Link>,
        earmark = <BookmarkToggle bookmark={{ title: announcement.title, to: announcementLink }} />,
        body =
          <>
            <p>
              <Link to={`/team/${announcement.author.slug}`}>
                {announcement.author.name}
              </Link> - {format(announcement.date, 'date-long')}
            </p>
            <p className="mb-0">{announcement.summary}</p>
          </>,
        footer = !!announcement.tags.length && <Tags label="Tags" tags={announcement.tags} />

  return <Card className={fullClassName} {...{ body, earmark, element, footer, title }} />;
}
