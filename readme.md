# VShift - Configs

## Usage

Add this repo as a project (dev) dependency: `yarn add git+https://bitbucket.org/vshift/vshift-configs.git`

### ESLint

Extend in ESLint config file:

```
extends:
  - ./node_modules/vshift-configs/configs/eslint-config-react.yml
```

*see https://github.com/eslint/eslint/issues/8644 for more info on the need for the explicit path*


### Stylelint

Extend in Stylelint config file:

```
extends:
  - ./node_modules/vshift-configs/configs/stylelint-config-scss.yml
```

### CSpell

Extend in CSpell config file:

```
const defaultConfig = require('./configs/cspell-config');


const projectConfig = {
  // words: [
  //   ...defaultConfig.words,
  // ],
};

module.exports = { ...defaultConfig, ...projectConfig };
```

